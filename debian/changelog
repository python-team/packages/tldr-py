tldr-py (0.7.0-8) unstable; urgency=medium

  * Drop dependency on python3-pkg-resources (Closes: #1083795)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 05 Feb 2025 20:43:37 +0100

tldr-py (0.7.0-7) unstable; urgency=medium

  * Add myself as human Uploader
  * Remove usage of python3-mock
  * "Set Rules-Requires-Root: no"
  * Bump Standards-Version to 4.7.0

  [ Ondřej Nový ]
  * Remove myself from Uploaders.

 -- Alexandre Detiste <tchet@debian.org>  Sun, 15 Dec 2024 01:50:35 +0100

tldr-py (0.7.0-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 13.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 25 Nov 2022 02:39:16 +0000

tldr-py (0.7.0-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on python3-mock and
      python3-pytest.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 16 Nov 2022 10:56:26 +0000

tldr-py (0.7.0-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/copyright: Bump my copyright year.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * debian/tests/control
    - mark autopkgtests as superficial; Closes: #971504

 -- Sandro Tosi <morph@debian.org>  Thu, 16 Sep 2021 23:22:04 -0400

tldr-py (0.7.0-3) unstable; urgency=medium

  * d/copyright: Bump my copyright year
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Set Vcs-* to salsa.debian.org
  * Add upstream metadata
  * Bump standards version to 4.2.1 (no changes)

 -- Ondřej Nový <onovy@debian.org>  Sat, 29 Sep 2018 18:34:20 +0200

tldr-py (0.7.0-2) unstable; urgency=medium

  * Bump debhelper compat level to 11
  * Bump Standards-Version to 4.1.3 (no changes needed)
  * Use update-alternatives for /usr/bin/tldr to prevent collision
    (Closes: #885793)

 -- Ondřej Nový <onovy@debian.org>  Fri, 05 Jan 2018 17:29:15 +0100

tldr-py (0.7.0-1) unstable; urgency=medium

  * Initial release. (Closes: #883065)

 -- Ondřej Nový <onovy@debian.org>  Wed, 29 Nov 2017 14:16:56 +0100
